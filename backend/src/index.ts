const  express = require('express');
import {Index} from '../src/controller/indexcontroller'
const bodyParser = require('body-parser')
import cors from 'cors'
const app = express();
const corsOptions ={
  origin:'http://localhost:3000', 
  credentials:true,            //access-control-allow-credentials:true
  optionSuccessStatus:200
}
app.use(cors(corsOptions))

app.use( bodyParser.json()); 
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 

const PORT = process.env.PORT;



app.get('/:code',async (req:any, res:any)=>{new Index().viewcode(req,res)})
app.get('/',async (req:any, res:any) => { new Index().views(req,res)});
app.post('/generate',async (req:any, res:any)=>{new Index().generate(req,res)})

app.listen(PORT, () => {
console.log(`Server is running at http://localhost:${PORT}`);
});