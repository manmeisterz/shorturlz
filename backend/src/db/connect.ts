// const { MongoClient, ServerApiVersion } = require('mongodb');
const mongoose = require('mongoose');
import * as dotenv from 'dotenv';
// const { Schema } = mongoose;
require('dotenv').config();
// console.log(process.env.mongodb!)
mongoose.connect(process.env.mongodb!)

const shortUrlSchema = new mongoose.Schema({
    _ID:  mongoose.Schema.Types.ObjectId,
    originurl: String,
    shorturl: String,
    codeurl: String
})

export const shortUrl = mongoose.model('shortUrl', shortUrlSchema);
