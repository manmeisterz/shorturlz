
import {shortUrl} from '../../src/db/connect'
import ip from 'ip'
require('dotenv').config();
function makeid(length:number) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
export class Index{

    async views(req:any,res:any){
    
        let data = await shortUrl.find({})
        res.send({data:data})
    }

    async generate(req:any,res:any){
 
        console.log(req.body)
        let {url} =  req.body
        let code = makeid(5);
        let link = `${req.protocol}://127.0.0.1:${process.env.PORT}/${code}`
        console.log(ip.address())
        await shortUrl.create({originurl:url,shorturl:link,codeurl:code})
        res.send({Url:link})
    }

    async viewcode (req:any,res:any){
    
        let {code} = req.params;
        if(!!code && code.length == 5){
            let redirectUrl:any = await shortUrl.findOne({codeurl:code})
            console.log(redirectUrl)
            res.redirect(`http://${redirectUrl.originurl}`)
        }else{
            res.send("NOTE FOUND 404")
        }
        // console.log()
    }

    

}