import React, { useState, useEffect } from "react";
import { Button, Form, InputGroup, Card, Row } from "react-bootstrap";
import axios from "axios";
import {CopyToClipboard} from 'react-copy-to-clipboard';
const Index = () => {
  var [data, setData] = useState("");
  var [shortUrl, setshortUrl] = useState("");
  const genarate = (value: any) => {
    axios
      .post("http://localhost:8000/generate", { url: value })
      .then((result: any) => {
        setshortUrl(result.data.Url);
      });
  };
  // useEffect(()=>)
  return (
    <div className="container h-100 w-100">
      <div className="row">
        {/* <div className='col'></div> */}
        <div className="col-md-6  position-absolute top-50 start-50 translate-middle ">
          <div>
            <InputGroup className="mb-3">
              <Form.Control
                placeholder="Url"
                aria-describedby="basic-addon2"
                onChange={(e) => setData(e.target.value)}
              />
              <Button
                variant="btn btn-primary"
                id="button-addon2"
                onClick={() => genarate(data)}
              >
                Genarate
              </Button>
            </InputGroup>
          </div>

          <div>
            <Card>
              <Card.Body>
                ลิงค์ที่ได้
                <InputGroup className="mb-3">
                  <Form.Control
                    placeholder="Url"
                    aria-describedby="basic-addon2"
                    defaultValue={shortUrl}
                  />
                  <CopyToClipboard text={shortUrl} >
                  <Button variant="btn btn-primary" id="button-addon2">
                    Copy
                  </Button>
                  </CopyToClipboard>
                </InputGroup>
              </Card.Body>
            </Card>
          </div>
        </div>
        {/* <div className='col'></div> */}
      </div>
    </div>
  );
};

export default Index;
